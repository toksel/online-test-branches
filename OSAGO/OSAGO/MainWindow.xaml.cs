﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace OSAGO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Copy_Button_Click(object sender, RoutedEventArgs e) // Копируем в буфер
        {

            string txt = "";
            switch (((Button)sender).Tag)
            {
                case "S1":
                    txt = S1.Text;
                    break;
                case "S2":
                    txt = S2.Text;
                    break;
                case "S3":
                    txt = S3.Text;
                    break;
                case "S4":
                    txt = S4.Text;
                    break;
                case "S5":
                    txt = S5.Text;
                    break;
                case "S6":
                    txt = S6.Text;
                    break;
                case "S7":
                    txt = S7.Text;
                    break;
                case "S8":
                    txt = S8.Text;
                    break;
                case "S9":
                    txt = S9.Text;
                    break;
                case "S10":
                    txt = S10.Text;
                    break;

                case "TS1":
                    txt = TS1.Text;
                    break;
                case "TS2":
                    txt = TS2.Text;
                    break;
                case "TS3":
                    txt = TS3.Text;
                    break;
                case "TS4":
                    txt = TS4.Text;
                    break;
                case "TS5":
                    txt = TS5.Text;
                    break;
                case "TS6":
                    txt = TS6.Text;
                    break;
                case "TS7":
                    txt = TS7.Text;
                    break;
                case "TS8":
                    txt = TS8.Text;
                    break;
                case "TS9":
                    txt = TS9.Text;
                    break;
                case "TS10":
                    txt = TS10.Text;
                    break;
                case "TS11":
                    txt = TS11.Text;
                    break;
                case "TS12":
                    txt = TS12.Text;
                    break;
                case "TS13":
                    txt = TS13.Text;
                    break;
                case "TS14":
                    txt = TS14.Text;
                    break;
                case "TS15":
                    txt = TS15.Text;
                    break;
                case "TS16":
                    txt = TS16.Text;
                    break;
                case "TS17":
                    txt = TS17.Text;
                    break;
                case "TS18":
                    txt = TS18.Text;
                    break;
                case "TS19":
                    txt = TS19.Text;
                    break;

                case "V1_1":
                    txt = V1_1.Text;
                    break;
                case "V1_2":
                    txt = V1_2.Text;
                    break;
                case "V1_3":
                    txt = V1_3.Text;
                    break;
                case "V1_4":
                    txt = V1_4.Text;
                    break;
                case "V1_5":
                    txt = V1_5.Text;
                    break;

                case "V2_1":
                    txt = V2_1.Text;
                    break;
                case "V2_2":
                    txt = V2_2.Text;
                    break;
                case "V2_3":
                    txt = V2_3.Text;
                    break;
                case "V2_4":
                    txt = V2_4.Text;
                    break;
                case "V2_5":
                    txt = V2_5.Text;
                    break;
            }
            txt = txt.Trim();
            Clipboard.SetText(txt);
        }

        private void Copy_to_Box_Button_Click(object sender, RoutedEventArgs e) // Копируем добавляем текст в окно суммы
        {

            string txt = "";
            switch (((Button)sender).Tag)
            {
                case "S1toBox":
                    txt = S1.Text;
                    break;
                case "S2toBox":
                    txt = S2.Text;
                    break;
                case "S3toBox":
                    txt = S3.Text;
                    break;
                case "S4toBox":
                    txt = S4.Text;
                    break;
                case "S5toBox":
                    txt = S5.Text;
                    break;
                case "S6toBox":
                    txt = S6.Text;
                    break;
                case "S7toBox":
                    txt = S7.Text;
                    break;
                case "S8toBox":
                    txt = S8.Text;
                    break;
                case "S9toBox":
                    txt = S9.Text;
                    break;
                case "S10toBox":
                    txt = S10.Text;
                    break;

                case "TS1toBox":
                    txt = TS1.Text;
                    break;
                case "TS2toBox":
                    txt = TS2.Text;
                    break;
                case "TS3toBox":
                    txt = TS3.Text;
                    break;
                case "TS4toBox":
                    txt = TS4.Text;
                    break;
                case "TS5toBox":
                    txt = TS5.Text;
                    break;
                case "TS6toBox":
                    txt = TS6.Text;
                    break;
                case "TS7toBox":
                    txt = TS7.Text;
                    break;
                case "TS8toBox":
                    txt = TS8.Text;
                    break;
                case "TS9toBox":
                    txt = TS9.Text;
                    break;
                case "TS10toBox":
                    txt = TS10.Text;
                    break;
                case "TS11toBox":
                    txt = TS11.Text;
                    break;
                case "TS12toBox":
                    txt = TS12.Text;
                    break;
                case "TS13toBox":
                    txt = TS13.Text;
                    break;
                case "TS14toBox":
                    txt = TS14.Text;
                    break;
                case "TS15toBox":
                    txt = TS15.Text;
                    break;
                case "TS16toBox":
                    txt = TS16.Text;
                    break;
                case "TS17toBox":
                    txt = TS17.Text;
                    break;
                case "TS18toBox":
                    txt = TS18.Text;
                    break;
                case "TS19toBox":
                    txt = TS19.Text;
                    break;

                case "V1_1toBox":
                    txt = V1_1.Text;
                    break;
                case "V1_2toBox":
                    txt = V1_2.Text;
                    break;
                case "V1_3toBox":
                    txt = V1_3.Text;
                    break;
                case "V1_4toBox":
                    txt = V1_4.Text;
                    break;
                case "V1_5toBox":
                    txt = V1_5.Text;
                    break;

                case "V2_1toBox":
                    txt = V2_1.Text;
                    break;
                case "V2_2toBox":
                    txt = V2_2.Text;
                    break;
                case "V2_3toBox":
                    txt = V2_3.Text;
                    break;
                case "V2_4toBox":
                    txt = V2_4.Text;
                    break;
                case "V2_5toBox":
                    txt = V2_5.Text;
                    break;
            }
            txt = txt.Trim();
            Sum.Text += txt + " ";
        }

        private void Sum_Clear_Button_Click(object sender, RoutedEventArgs e)
        {
            Sum.Text = "";
        }

        private void Sum_Copy_Button_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(Sum.Text);
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog save_dlg = new Microsoft.Win32.SaveFileDialog();
            save_dlg.FileName = "Document";
            save_dlg.DefaultExt = ".txt";
            save_dlg.Filter = "Text documents (.txt)|*.txt";
            if (save_dlg.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(save_dlg.OpenFile(), Encoding.Default))
                {
                    sw.WriteLine(S1.Text);
                    sw.WriteLine(S2.Text);
                    sw.WriteLine(S3.Text);
                    sw.WriteLine(S4.Text);
                    sw.WriteLine(S5.Text);
                    sw.WriteLine(S6.Text);
                    sw.WriteLine(S7.Text);
                    sw.WriteLine(S8.Text);
                    sw.WriteLine(S9.Text);
                    sw.WriteLine(S10.Text);

                    sw.WriteLine(TS1.Text);
                    sw.WriteLine(TS2.Text);
                    sw.WriteLine(TS3.Text);
                    sw.WriteLine(TS4.Text);
                    sw.WriteLine(TS5.Text);
                    sw.WriteLine(TS6.Text);
                    sw.WriteLine(TS7.Text);
                    sw.WriteLine(TS8.Text);
                    sw.WriteLine(TS9.Text);
                    sw.WriteLine(TS10.Text);
                    sw.WriteLine(TS11.Text);
                    sw.WriteLine(TS12.Text);
                    sw.WriteLine(TS13.Text);
                    sw.WriteLine(TS14.Text);
                    sw.WriteLine(TS15.Text);
                    sw.WriteLine(TS16.Text);
                    sw.WriteLine(TS17.Text);
                    sw.WriteLine(TS18.Text);
                    sw.WriteLine(TS19.Text);

                    sw.WriteLine(V1_1.Text);
                    sw.WriteLine(V1_2.Text);
                    sw.WriteLine(V1_3.Text);
                    sw.WriteLine(V1_4.Text);
                    sw.WriteLine(V1_5.Text);

                    sw.WriteLine(V2_1.Text);
                    sw.WriteLine(V2_2.Text);
                    sw.WriteLine(V2_3.Text);
                    sw.WriteLine(V2_4.Text);
                    sw.WriteLine(V2_5.Text);

                    /*
                    String[] data = { S1.Text, S2.Text, S3.Text, S4.Text, S5.Text, S6.Text, S7.Text, S8.Text, S9.Text, S10.Text, TS1.Text, TS2.Text, TS3.Text, TS4.Text, TS6.Text, TS7.Text,
                        TS8.Text, TS9.Text, TS10.Text, TS11.Text, TS12.Text, TS13.Text, TS14.Text, TS15.Text, TS16.Text, TS17.Text, TS18.Text, TS19.Text, V1_1.Text, V1_2.Text, V1_3.Text, V1_4.Text,
                        V1_5.Text, V2_1.Text, V2_2.Text, V2_3.Text, V2_4.Text, V2_5.Text};
                    foreach (string d in data)
                    {
                        sw.WriteLine(d);
                    }
                    */
                }
            }
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog load_dlg = new Microsoft.Win32.OpenFileDialog();
            load_dlg.FileName = "Document";
            load_dlg.DefaultExt = ".txt";
            load_dlg.Filter = "Text documents (.txt)|*.txt";
            if (load_dlg.ShowDialog() == true)
            {
                FileInfo fi = new FileInfo(load_dlg.FileName);
                using (StreamReader sr = new StreamReader(fi.Open(FileMode.Open, FileAccess.Read), Encoding.Default))
                {
                    /*
                    String[] data = { S1.Text, S2.Text, S3.Text, S4.Text, S5.Text, S6.Text, S7.Text, S8.Text, S9.Text, S10.Text, TS1.Text, TS2.Text, TS3.Text, TS4.Text, TS6.Text, TS7.Text,
                        TS8.Text, TS9.Text, TS10.Text, TS11.Text, TS12.Text, TS13.Text, TS14.Text, TS15.Text, TS16.Text, TS17.Text, TS18.Text, TS19.Text, V1_1.Text, V1_2.Text, V1_3.Text, V1_4.Text,
                        V1_5.Text, V2_1.Text, V2_2.Text, V2_3.Text, V2_4.Text, V2_5.Text};
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] = sr.ReadLine();
                    }
                    */
                    S1.Text = sr.ReadLine();
                    S2.Text = sr.ReadLine();
                    S3.Text = sr.ReadLine();
                    S4.Text = sr.ReadLine();
                    S5.Text = sr.ReadLine();
                    S6.Text = sr.ReadLine();
                    S7.Text = sr.ReadLine();
                    S8.Text = sr.ReadLine();
                    S9.Text = sr.ReadLine();
                    S10.Text = sr.ReadLine();

                    TS1.Text = sr.ReadLine();
                    TS2.Text = sr.ReadLine();
                    TS3.Text = sr.ReadLine();
                    TS4.Text = sr.ReadLine();
                    TS5.Text = sr.ReadLine();
                    TS6.Text = sr.ReadLine();
                    TS7.Text = sr.ReadLine();
                    TS8.Text = sr.ReadLine();
                    TS9.Text = sr.ReadLine();
                    TS10.Text = sr.ReadLine();
                    TS11.Text = sr.ReadLine();
                    TS12.Text = sr.ReadLine();
                    TS13.Text = sr.ReadLine();
                    TS14.Text = sr.ReadLine();
                    TS15.Text = sr.ReadLine();
                    TS16.Text = sr.ReadLine();
                    TS17.Text = sr.ReadLine();
                    TS18.Text = sr.ReadLine();
                    TS19.Text = sr.ReadLine();

                    V1_1.Text = sr.ReadLine();
                    V1_2.Text = sr.ReadLine();
                    V1_3.Text = sr.ReadLine();
                    V1_4.Text = sr.ReadLine();
                    V1_5.Text = sr.ReadLine();

                    V2_1.Text = sr.ReadLine();
                    V2_2.Text = sr.ReadLine();
                    V2_3.Text = sr.ReadLine();
                    V2_4.Text = sr.ReadLine();
                    V2_5.Text = sr.ReadLine();
                }

                /*
                StreamReader sr = new StreamReader(fi.Open(FileMode.Open, FileAccess.Read), Encoding.GetEncoding(1251));
                String[] data = { S1.Text, S2.Text, S3.Text, S4.Text, S5.Text, S6.Text, S7.Text, S8.Text, S9.Text, S10.Text, TS1.Text, TS2.Text, TS3.Text, TS4.Text, TS6.Text, TS7.Text,
                        TS8.Text, TS9.Text, TS10.Text, TS11.Text, TS12.Text, TS13.Text, TS14.Text, TS15.Text, TS16.Text, TS17.Text, TS18.Text, TS19.Text, V1_1.Text, V1_2.Text, V1_3.Text, V1_4.Text,
                        V1_5.Text, V2_1.Text, V2_2.Text, V2_3.Text, V2_4.Text, V2_5.Text};
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = sr.ReadLine();
                }
                sr.Close();
                */
            }
        }

    }
}
